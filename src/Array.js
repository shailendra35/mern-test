import React, { Component } from "react";
import Counter from "./Counter";
import "./App.css";
export default class App extends Component {
  state = {
    count: 0,
    message: "Count cannot be less than 0",
  };

  render() {
    return (
      <>
       <div className="sub">
        <div className="container">
       
          <div className="add">
            <button
              onClick={() => {
                this.setState({ count: this.state.count + 1 });
              }}
            >
              Increase
            </button>
          </div>
          <div className="val">
            <Counter count={this.state.count} />
          </div>
          <div className="min">
            <button
              onClick={() => {
                if (this.state.count > 0) {
                  this.setState({ count: this.state.count - 1 });
                }
              }}
            >
              Decrease
            </button>
          </div>
          <div>{this.state.count <= 0 ? <p id="message">{this.state.message}</p> : null}</div>
        </div>
        </div>
      </>
    );
  }
}
