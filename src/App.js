import React, { Component } from "react";
import Counter from "./Counter";
import "./App.css";

export default class App extends Component {
  state = {
    personalInformation:[
      {
        name: "Shailendra",
        address: "Bkt",
        phoneNumber: "9861290648"
      },
      {
        name: "Anmol",
        address: "Bkt",
        phoneNumber: "9861264648"
      },
      {name: "Surya",
      address: "ktm",
      phoneNumber: "9861789648"
      }
    ]

  };

  render() {
    return (
      <>
       <div className="sub">

        <h1>Array Map</h1>
        {this.state.personalInformation.map((item,i)=> {
          return(
            <div key={i}>
              <h1>Name is:{item.name}</h1>
              <h2>Address is:{item. address}</h2>
              <h2>Contact No is:{item.phoneNumber}</h2>


            </div>
          )
        })}
        
        </div>
      </>
    );
  }
}
